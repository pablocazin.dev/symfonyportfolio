<?php

namespace App\Controller;

use App\Entity\ProjetTechnos;
use App\Form\ProjetTechnosType;
use App\Repository\ProjetTechnosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/projet-technos')]
class ProjetTechnosController extends AbstractController
{
    #[Route('/', name: 'app_projet_technos_index', methods: ['GET'])]
    public function index(ProjetTechnosRepository $projetTechnosRepository): Response
    {
        return $this->render('projet_technos/index.html.twig', [
            'projet_technos' => $projetTechnosRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_projet_technos_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProjetTechnosRepository $projetTechnosRepository): Response
    {
        $projetTechno = new ProjetTechnos();
        $form = $this->createForm(ProjetTechnosType::class, $projetTechno);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projetTechnosRepository->add($projetTechno);
            return $this->redirectToRoute('app_projet_technos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projet_technos/new.html.twig', [
            'projet_techno' => $projetTechno,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_projet_technos_show', methods: ['GET'])]
    public function show(ProjetTechnos $projetTechno): Response
    {
        return $this->render('projet_technos/show.html.twig', [
            'projet_techno' => $projetTechno,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_projet_technos_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ProjetTechnos $projetTechno, ProjetTechnosRepository $projetTechnosRepository): Response
    {
        $form = $this->createForm(ProjetTechnosType::class, $projetTechno);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projetTechnosRepository->add($projetTechno);
            return $this->redirectToRoute('app_projet_technos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projet_technos/edit.html.twig', [
            'projet_techno' => $projetTechno,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_projet_technos_delete', methods: ['POST'])]
    public function delete(Request $request, ProjetTechnos $projetTechno, ProjetTechnosRepository $projetTechnosRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$projetTechno->getId(), $request->request->get('_token'))) {
            $projetTechnosRepository->remove($projetTechno);
        }

        return $this->redirectToRoute('app_projet_technos_index', [], Response::HTTP_SEE_OTHER);
    }
}
