<?php

namespace App\Controller;

use App\Entity\ProjetContent;
use App\Form\ProjetContentType;
use App\Repository\ProjetContentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/projet/content')]
class ProjetContentController extends AbstractController
{
    #[Route('/', name: 'app_projet_content_index', methods: ['GET'])]
    public function index(ProjetContentRepository $projetContentRepository): Response
    {
        return $this->render('projet_content/index.html.twig', [
            'projet_contents' => $projetContentRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_projet_content_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProjetContentRepository $projetContentRepository): Response
    {
        $projetContent = new ProjetContent();
        $form = $this->createForm(ProjetContentType::class, $projetContent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projetContentRepository->add($projetContent);
            return $this->redirectToRoute('app_projet_content_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projet_content/new.html.twig', [
            'projet_content' => $projetContent,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_projet_content_show', methods: ['GET'])]
    public function show(ProjetContent $projetContent): Response
    {
        return $this->render('projet_content/show.html.twig', [
            'projet_content' => $projetContent,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_projet_content_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ProjetContent $projetContent, ProjetContentRepository $projetContentRepository): Response
    {
        $form = $this->createForm(ProjetContentType::class, $projetContent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projetContentRepository->add($projetContent);
            return $this->redirectToRoute('app_projet_content_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projet_content/edit.html.twig', [
            'projet_content' => $projetContent,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_projet_content_delete', methods: ['POST'])]
    public function delete(Request $request, ProjetContent $projetContent, ProjetContentRepository $projetContentRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$projetContent->getId(), $request->request->get('_token'))) {
            $projetContentRepository->remove($projetContent);
        }

        return $this->redirectToRoute('app_projet_content_index', [], Response::HTTP_SEE_OTHER);
    }
}
