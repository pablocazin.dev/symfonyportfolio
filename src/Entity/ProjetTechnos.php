<?php

namespace App\Entity;

use App\Repository\ProjetTechnosRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjetTechnosRepository::class)]
class ProjetTechnos
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Projet::class, inversedBy: 'projetTechnos')]
    #[ORM\JoinColumn(nullable: false)]
    private $id_projet;

    #[ORM\ManyToOne(targetEntity: Technos::class, inversedBy: 'projetTechnos')]
    #[ORM\JoinColumn(nullable: false)]
    private $id_tech;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdProjet(): ?Projet
    {
        return $this->id_projet;
    }

    public function setIdProjet(?Projet $id_projet): self
    {
        $this->id_projet = $id_projet;

        return $this;
    }

    public function getIdTech(): ?Technos
    {
        return $this->id_tech;
    }

    public function setIdTech(?Technos $id_tech): self
    {
        $this->id_tech = $id_tech;

        return $this;
    }
}
