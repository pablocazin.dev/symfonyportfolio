<?php

namespace App\Entity;

use App\Repository\ProjetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjetRepository::class)]
class Projet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $nom;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $line;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $url;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $git;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $image_card;

    #[ORM\OneToMany(mappedBy: 'id_projet', targetEntity: ProjetContent::class)]
    public $projetContents;

    #[ORM\OneToMany(mappedBy: 'id_projet', targetEntity: ProjetTechnos::class)]
    private $projetTechnos;

    public function __construct()
    {
        $this->projetContents = new ArrayCollection();
        $this->projetTechnos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLine(): ?string
    {
        return $this->line;
    }

    public function setLine(?string $line): self
    {
        $this->line = $line;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getGit(): ?string
    {
        return $this->git;
    }

    public function setGit(?string $git): self
    {
        $this->git = $git;

        return $this;
    }

    public function getImageCard(): ?string
    {
        return $this->image_card;
    }

    public function setImageCard(?string $image_card): self
    {
        $this->image_card = $image_card;

        return $this;
    }

    /**
     * @return Collection<int, ProjetContent>
     */
    public function getProjetContents(): Collection
    {
        return $this->projetContents;
    }

    public function addProjetContent(ProjetContent $projetContent): self
    {
        if (!$this->projetContents->contains($projetContent)) {
            $this->projetContents[] = $projetContent;
            $projetContent->setIdProjet($this);
        }

        return $this;
    }

    public function removeProjetContent(ProjetContent $projetContent): self
    {
        if ($this->projetContents->removeElement($projetContent)) {
            // set the owning side to null (unless already changed)
            if ($projetContent->getIdProjet() === $this) {
                $projetContent->setIdProjet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProjetTechnos>
     */
    public function getProjetTechnos(): Collection
    {
        return $this->projetTechnos;
    }

    public function addProjetTechno(ProjetTechnos $projetTechno): self
    {
        if (!$this->projetTechnos->contains($projetTechno)) {
            $this->projetTechnos[] = $projetTechno;
            $projetTechno->setIdProjet($this);
        }

        return $this;
    }

    public function removeProjetTechno(ProjetTechnos $projetTechno): self
    {
        if ($this->projetTechnos->removeElement($projetTechno)) {
            // set the owning side to null (unless already changed)
            if ($projetTechno->getIdProjet() === $this) {
                $projetTechno->setIdProjet(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
