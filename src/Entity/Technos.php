<?php

namespace App\Entity;

use App\Repository\TechnosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TechnosRepository::class)]
class Technos
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $nom;

    #[ORM\OneToMany(mappedBy: 'id_tech', targetEntity: ProjetTechnos::class)]
    private $projetTechnos;

    public function __construct()
    {
        $this->projetTechnos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, ProjetTechnos>
     */
    public function getProjetTechnos(): Collection
    {
        return $this->projetTechnos;
    }

    public function addProjetTechno(ProjetTechnos $projetTechno): self
    {
        if (!$this->projetTechnos->contains($projetTechno)) {
            $this->projetTechnos[] = $projetTechno;
            $projetTechno->setIdTech($this);
        }

        return $this;
    }

    public function removeProjetTechno(ProjetTechnos $projetTechno): self
    {
        if ($this->projetTechnos->removeElement($projetTechno)) {
            // set the owning side to null (unless already changed)
            if ($projetTechno->getIdTech() === $this) {
                $projetTechno->setIdTech(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
