<?php

namespace App\Repository;

use App\Entity\Technos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Technos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Technos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Technos[]    findAll()
 * @method Technos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TechnosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Technos::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Technos $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Technos $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Technos[] Returns an array of Technos objects
    //  */
    
    public function findByProject($value)
    {
        return $this->getEntityManager()
        ->createQuery('
            SELECT t.nom FROM App\Entity\Technos t
            join App\Entity\ProjetTechnos p 
            WITH t.id = p.id_tech
            WHERE p.id_projet = :id
        ')
        ->setParameter('id', $value)
        ->getResult()
        ;
    }

    

    /*
    public function findOneBySomeField($value): ?Technos
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
